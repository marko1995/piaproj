export default function addHeaders(res) {
	res.setHeader(
		"Access-Control-Allow-Origin",
		"*"
	)
	res.setHeader(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept"
	)
	res.setHeader(
		"Access-Control-Allow-Methods",
		"GET, POST, PATCH, DELETE, OPTIONS, PUT"
	)
}
