import jwt from "jsonwebtoken"

function verifyToken(token) {
	try {
		const data = jwt.verify(token, process.env.TOKEN_SECRET)
		return {...data}
	} catch (e) {
		return undefined
	}
}

function signToken({...params}) {
	try {
		return jwt.sign(params, process.env.TOKEN_SECRET)
	} catch (e) {
		return undefined
	}
}

export {verifyToken, signToken}