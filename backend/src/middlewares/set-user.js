export default function setUser({verifyToken}) {
	return ({req}) => {
		const token = req.headers.authorization

		if (!token) {
			throw {
				code: 403,
				message: "Missing Authorization Header"
			}
		}

		const userData = verifyToken(token)

		if (!userData || !userData.userType || !userData.id || !userData.username) {
			throw {
				code: 403,
				message: "Invalid Authorization Header"
			}
		}

		req.id = userData.id
		req.userType = userData.userType
		req.username = userData.username
	}
}
