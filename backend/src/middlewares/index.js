import setUser from "./set-user"
import checkUserRole from "./check-role"
import {verifyToken} from "../token"

export default Object.freeze({
	setUser: setUser({verifyToken}),
	checkUserRole: checkUserRole
})
