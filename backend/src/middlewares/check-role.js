export default function checkUserRole(roles) {
	return ({req}) => {
		if (!req.userType || !(roles.indexOf(req.userType) >= 0)) {
			throw {
				code: 401,
				message: "userType missing"
			}
		}
	}
}
