export default function makeMiddleWareCallback(middleware) {
	return (req, res, next) => {
		try {
			middleware({req, res})
			next()
		} catch (e) {
			if (e instanceof Error) {
				res.status(500).send({error: "An unknown error occurred."})
			} else {
				const {code, message} = e
				res.status(code).send({error: message})
			}
		}
	}
}
