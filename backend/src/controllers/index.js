import userController from "./user"


const controllers = Object.freeze({
	userController
})

export default controllers
