export default function makeFindUser({findUserUseCase, userErrors}) {
	return async function findUser(httpRequest) {
		try {
			const id = httpRequest.id
			const user = await findUserUseCase({id})

			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 200,
				body: {user}
			}
		} catch (e) {
			// TODO: Error logging
			console.log(e)

			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 400,
				body: {
					error: e.message
				}
			}
		}
	}
}
