export default function makeRegistration({addUserUseCase}) {
	return async function registration(httpRequest) {
		try {
			const userInfo = httpRequest.body
			const user = await addUserUseCase({...userInfo})

			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 201,
				body: {user}
			}
		} catch (e) {
			// TODO: Error logging
			console.log(e)

			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 400,
				body: {
					error: e.message
				}
			}
		}
	}
}
