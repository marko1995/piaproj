export default function makeLogin({findUserUseCase, userErrors, signToken}) {
	return async function login(httpRequest) {
		try {
			const body = httpRequest.body
			const user = await findUserUseCase({...body})
			const token = signToken({
				username: user.username,
				userType: user.userType,
				id: user.id
			})
			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 200,
				body: {token}
			}
		} catch (e) {
			// TODO: Error logging
			console.log(e)

			return {
				headers: {
					"Content-Type": "application/json"
				},
				statusCode: 400,
				body: {
					error: e.message
				}
			}
		}
	}
}
