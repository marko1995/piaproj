import userUseCase from "../../use-cases/user"

import makeAddUser from "./registration"
import makeLogin from "./login"
import makeFindUser from "./find-user"
import {userErrors} from "../../models/user"
import {signToken} from "../../token"

const registration = makeAddUser({addUserUseCase: userUseCase.addUser})
const login = makeLogin({findUserUseCase: userUseCase.findUser, userErrors, signToken})
const findUser = makeFindUser({findUserUseCase: userUseCase.findUser, userErrors})

const userController = Object.freeze({
	registration,
	login,
	findUser
})

export default userController
