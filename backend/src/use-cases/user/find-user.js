// user-usecase -> find-user
import {makeUser} from "../../models/user"
import userRole from "../../roles"
import {makeFarmer} from "../../models/farmer"

export default function makeFindUser({userDb, farmerDb}) {
	return async function findUser({id, username, password}) {
		let user
		if(id) {
			user = await userDb.findById({id})
		} else {
			user = await userDb.find({username, password})
		}
		user = makeUser(user)

		let specificUser

		switch (user.getUserType()) {
		case userRole.ADMIN:
			specificUser = undefined
			break
		case userRole.COMPANY:
			specificUser = undefined
			break
		case userRole.FARMER:
			specificUser = makeFarmer(farmerDb.findById({id}))
			break
		}


		return {...user.getObject(), ...specificUser.getObject()}
	}
}
