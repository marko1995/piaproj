// user-usecase
import makeAddUser from "./add-user"
import makeFindUser from "./find-user"
import userDb from "../../data-access/user"
import farmerDb from "../../data-access/farmer"

const addUser = makeAddUser({userDb, farmerDb})
const findUser = makeFindUser({userDb, farmerDb})

const userUseCase = Object.freeze({
	addUser,
	findUser
})

export default userUseCase
