// user-usecase -> add-user
import {makeUser} from "../../models/user"
import {makeFarmer} from "../../models/farmer"
import userRole from "../../roles"

export default function makeAddUser({userDb, farmerDb}) {
	return async function addUser(userInfo) {
		let user = makeUser(userInfo)
		let specificUser

		switch (user.getUserType()) {
		case userRole.ADMIN:
			specificUser = undefined
			break
		case userRole.COMPANY:
			specificUser = undefined
			break
		case userRole.FARMER:
			specificUser = makeFarmer(userInfo)
			break
		}

		user = makeUser(await userDb.insert({...user.getObjectWithPassword()}))

		switch (user.getUserType()) {
		case userRole.ADMIN: {
			specificUser = undefined
			break
		}
		case userRole.COMPANY: {
			specificUser = undefined
			break
		}
		case userRole.FARMER: {
			let farmer = makeFarmer(await farmerDb.insert({id: user.getId(), ...specificUser.getObject()}))
			specificUser = {...farmer.getObject()}
			break
		}
		}

		return {...user.getObject(), ...specificUser}
	}
}
