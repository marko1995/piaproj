import userRole from "../../roles"

export default function userRoute({controllers, responseCallback, router, middleWares, middleWareCallback}) {
	const userRouter = router()
	const userController = controllers.userController
	userRouter.post("/registration", responseCallback(userController.registration))
	userRouter.post("/login", responseCallback(userController.login))
	userRouter.post("",
		middleWareCallback(middleWares.setUser),
		middleWareCallback(middleWares.checkUserRole([userRole.ADMIN, userRole.FARMER, userRole.COMPANY])),
		responseCallback(userController.findUser)
	)
	return userRouter
}
