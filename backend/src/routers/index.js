import {Router} from "express"
import userRoute from "./user"
import {makeMiddleWareCallback, makeResponseCallback} from "../callbacks"
import controllers from "../controllers"
import middleWares from "../middlewares"

const args = {
	controllers,
	responseCallback: makeResponseCallback,
	router: Router,
	middleWares,
	middleWareCallback: makeMiddleWareCallback
}

const routers = Router()

routers.use("/user", userRoute(args))

export default routers