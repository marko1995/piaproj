export default function buildMakeFarmer() {
	return function makeFarmer({
		name,
		lastName,
		dob,
		birthplace,
		phone,
		email,
	}) {

		return Object.freeze({
			getName: () => name,
			getLastName: () => lastName,
			getDob: () => dob,
			getBirthplace: () => birthplace,
			getPhone: () => phone,
			getEmail: () => email,
			getObject: () => getObject()
		})

		function getObject() {
			return {
				name: name,
				lastName: lastName,
				dob: dob,
				birthplace: birthplace,
				phone: phone,
				email: email
			}
		}
	}
}
