import userRole from "../../roles"

export default function buildMakeUser({userErrors}) {
	return function makeUser({
		id,
		username,
		password,
		userType
	}) {
		// if (checks.isValidName(username)) {
		// 	throw Error("Username is not valid!")
		// }
		// if (checks.isValidName(name)) {
		// 	throw Error("Name is not valid!")
		// }
		// if (checks.isValidName(lastName)) {
		// 	throw Error("lastName is not valid!")
		// }
		// if (checks.isValidPassword(password)) {
		// 	throw Error("Password is not valid!")
		// }

		if (!checkRole(userType)) {
			throw new userErrors.UserTypeError()
		}

		return Object.freeze({
			getId: () => id,
			getUsername: () => username,
			getPassword: () => password,
			getUserType: () => userType,
			getObject: () => getObject(),
			getObjectWithPassword: () => getObjectWithPassword()
		})

		function getObject() {
			return {
				id: id,
				username: username,
				userType: userType
			}
		}

		function getObjectWithPassword() {
			return {
				id: id,
				username: username,
				userType: userType,
				password: password
			}
		}

		function checkRole(userType) {
			if (userType) {
				for (let value of Object.values(userRole)) {
					if (userType === value)
						return true
				}
			}
			return false
		}
	}
}
