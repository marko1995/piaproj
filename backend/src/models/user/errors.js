class UserTypeError extends Error {
	constructor(message) {
		super(message)
		this.name = "UserTypeError"
		Error.captureStackTrace(this, UserTypeError)
	}
}

class NotExistsError extends Error {
	constructor(message) {
		super(message)
		this.name = "NotExistsError"
		Error.captureStackTrace(this, NotExistsError)
	}
}

class PasswordIncorrectError extends Error {
	constructor(message) {
		super(message)
		this.name = "PasswordIncorrectError"
		Error.captureStackTrace(this, PasswordIncorrectError)
	}
}

class UsernameIncorrectError extends Error {
	constructor(message) {
		super(message)
		this.name = "UsernameIncorrectError"
		Error.captureStackTrace(this, UsernameIncorrectError)
	}
}

class UserErrors extends Error {
	constructor(errors, message) {
		super(message)
		this.name = "UserErrors"
		this.errors = errors
		Error.captureStackTrace(this, UserErrors)
	}
}

export default Object.freeze({
	UserTypeError,
	NotExistsError,
	PasswordIncorrectError,
	UsernameIncorrectError,
	UserErrors
})
