import buildMakeUser from "./user"
import userErrors from "./errors"

const makeUser = buildMakeUser({userErrors})

export {makeUser, userErrors}
