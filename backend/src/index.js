import express from "express"
import bodyParser from "body-parser"
import dotEnv from "dotenv"
import mongoose from "mongoose"
import routers from "./routers"
import addHeaders from "./headers"

dotEnv.config()

mongoose.connect(`${process.env.MONGO_STRING}`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(() => {
	console.log("Connected to database success!")
}).catch(() => {
	console.log("Connected to database failed!")
})

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use((req, res, next) => {
	addHeaders(res)
	next()
})

const apiRoot = process.env.FARMER_API_ROOT
app.use(`${apiRoot}`, routers)

app.listen(process.env.PORT, () => {
	console.log(`Server running ${process.env.PORT}`)
})
