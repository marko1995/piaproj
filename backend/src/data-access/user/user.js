export default function makeUserDb({userDb}) {
	return Object.freeze({
		insert,
		find,
		findById
	})

	async function insert({id: _id, ...user}) {
		const createdUser = (await userDb.create({_id, ...user})).toObject()
		const {_id: id, ...data} = createdUser
		return {id, ...data}
	}

	async function find({...params}) {
		const user = (await userDb.findOne({...params}))
		if(user) {
			const { _id: id, ...data } = user.toObject()
			return {id, ...data}
		}
		return {}
	}
	async function findById({id}) {
		const user = (await userDb.findById(id))
		if(user) {
			const { _id: id, ...data } = user.toObject()
			return {id, ...data}
		}
		return {}
	}
}
