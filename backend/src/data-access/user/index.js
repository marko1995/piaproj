import userModel from "./userModel"
import makeUserDb from "./user"

const userDb = makeUserDb({userDb: userModel})

export default userDb
