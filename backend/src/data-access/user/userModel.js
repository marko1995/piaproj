import mongoose from "mongoose"

const userSchema = mongoose.Schema(
	{
		username: String,
		password: String,
		userType: String // admin, farmer, company
	},
	{
		timestamps: {
			createdAt: "created_at",
			updatedAt: "updated_at"
		},
		versionKey: false
	}
)

const userModel = mongoose.model("User", userSchema)

export default userModel
