import farmerModel from "./farmerModel"
import makeFarmerDb from "./farmer"

const farmerDb = makeFarmerDb({farmerDb: farmerModel})

export default farmerDb