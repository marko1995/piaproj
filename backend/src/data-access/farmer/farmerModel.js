import mongoose from "mongoose"

const farmerSchema = mongoose.Schema(
	{
		_id: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
		name: String,
		lastName: String,
		phone: String,
		email: String,
		dob: String,
		birthplace: String
	},
	{
		timestamps: {
			createdAt: "created_at",
			updatedAt: "updated_at"
		},
		versionKey: false
	}
)

const farmerModel = mongoose.model("Farmer", farmerSchema)

export default farmerModel