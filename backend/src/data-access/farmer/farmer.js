export default function makeFarmerDb({farmerDb}) {
	return Object.freeze({
		insert,
		find,
		findById
	})

	async function insert({id: _id, ...farmer}) {
		const createdFarmer = (await farmerDb.create({_id, ...farmer})).toObject()
		const {_id: id, ...data} = createdFarmer
		return {id, ...data}
	}

	async function find({...params}) {
		const farmer = (await farmerDb.findOne({...params}))
		if(farmer) {
			const { _id: id, ...data } = farmer.toObject()
			return {id, ...data}
		}
		return {}
	}
	async function findById({id}) {
		const farmer = (await farmerDb.findById(id))
		if(farmer) {
			const { _id: id, ...data } = farmer.toObject()
			return {id, ...data}
		}
		return {}
	}
}
